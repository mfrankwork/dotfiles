When starting from scratch:

```bash
export DOT_USER=$(echo $USER | awk -F '\' '{print $2}')

mkdir -p ~/team_repos/dotfiles/$DOT_USER/bash ~/team_repos/dotfiles/$DOT_USER/vscode/snippets

cd ~
cp .bashrc ~/team_repos/dotfiles/$DOT_USER/bash/bashrc
cp .bash_profile ~/team_repos/dotfiles/$DOT_USER/bash/bash_profile
cp .gitconfig ~/team_repos/dotfiles/$DOT_USER/bash/gitconfig

cd ~
cp .config/Code/User/keybindings.json ~/team_repos/dotfiles/$DOT_USER/vscode/keybindings.json
touch ~/team_repos/dotfiles/$DOT_USER/vscode/keybindings.json
cp .config/Code/User/settings.json ~/team_repos/dotfiles/$DOT_USER/vscode/settings.json
touch ~/team_repos/dotfiles/$DOT_USER/vscode/settings.json
for f in ~/.config/Code/User/snippets/*
do
  echo "Copying $f"
  cp $f ~/team_repos/dotfiles/$DOT_USER/vscode/snippets/$(basename $f)
done

touch ~/team_repos/dotfiles/$DOT_USER/vscode/extensions.txt
```

==============================================

Generate list of Visual Studio Code extensions:

```bash
code --list-extensions | xargs -L 1 echo code --install-extension > ~/team_repos/dotfiles/$DOT_USER/vscode/extensions.txt
```

==============================================

To establish a new environment:

```bash
export DOT_USER=$(echo $USER | awk -F '\' '{print $2}')

cd ~
ln -nfs ~/team_repos/dotfiles/$DOT_USER/bash/bashrc .bashrc
ln -nfs ~/team_repos/dotfiles/$DOT_USER/bash/bash_profile .bash_profile
ln -nfs ~/team_repos/dotfiles/$DOT_USER/bash/gitconfig .gitconfig

cd ~
ln -nfs ~/team_repos/dotfiles/$DOT_USER/vscode/keybindings.json ~/.config/Code/User/keybindings.json
ln -nfs ~/team_repos/dotfiles/$DOT_USER/vscode/settings.json ~/.config/Code/User/settings.json
for f in ~/team_repos/dotfiles/$DOT_USER/vscode/snippets/*
do
  echo "Linking $f"
  ln -nfs $f ~/.config/Code/User/snippets/$(basename $f)
done

source ~/team_repos/dotfiles/$DOT_USER/vscode/extensions.txt
```
